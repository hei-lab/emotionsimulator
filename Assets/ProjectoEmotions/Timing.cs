﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text;
using System;
using System.Runtime.Serialization.Formatters.Binary;

public class Timing : MonoBehaviour
{
  [SerializeField] private float currentTime = 0f; //contador
    //homem branco
    public float timeHBN; //neutro
    [SerializeField] private Text textHBN; //UI onde vai afixar
    public float timeHBR; //raiva
    [SerializeField] private Text textHBR; //UI onde vai afixar
    public float timeHBM; //medo
    [SerializeField] private Text textHBM; //UI onde vai afixar
    public float timeHBF; //felicidade
    [SerializeField] private Text textHBF; //UI onde vai afixar

    public bool[] isHB; //bools
    //
    //homem negro
    public float timeHNN; //neutro
    [SerializeField] private Text textHNN; //UI onde vai afixar
    public float timeHNR; //raiva
    [SerializeField] private Text textHNR; //UI onde vai afixar
    public float timeHNM; //medo
    [SerializeField] private Text textHNM; //UI onde vai afixar
    public float timeHNF; //felicidade
    [SerializeField] private Text textHNF; //UI onde vai afixar

    public bool[] isHN; //bools
    //
    //mulher branca
    public float timeMBN; //neutro
    [SerializeField] private Text textMBN; //UI onde vai afixar
    public float timeMBR; //raiva
    [SerializeField] private Text textMBR; //UI onde vai afixar
    public float timeMBM; //medo
    [SerializeField] private Text textMBM; //UI onde vai afixar
    public float timeMBF; //felicidade
    [SerializeField] private Text textMBF; //UI onde vai afixar

    public bool[] isMB; //bools
    //
    //mulher negra
    public float timeMNN; //neutro
    [SerializeField] private Text textMNN; //UI onde vai afixar
    public float timeMNR; //raiva
    [SerializeField] private Text textMNR; //UI onde vai afixar
    public float timeMNM; //medo
    [SerializeField] private Text textMNM; //UI onde vai afixar
    public float timeMNF; //felicidade
    [SerializeField] private Text textMNF; //UI onde vai afixar

    public bool[] isMN; //bools
    //

    [SerializeField] private Login logininstance; //buscar dados do login
    [SerializeField] private string resultspath; //escolher caminho po resultado em txt.
    [SerializeField] private string buildpath = Application.persistentDataPath + "/results";


    void Start()
    {
        FileGenerator(); //gerar ficheiro com dados finais
    }

    void Update()
    {
        logininstance = GameObject.Find("Login").GetComponent<Login>(); //buscar dados do login

        currentTime += Time.deltaTime;
    }

    //converter o resultado para um UI text
    public void TimerCount()
    {
        currentTime += Time.deltaTime; //inicializar o timer

       //homem branco
       if(isHB[0] == true) //neutro
        {
            timeHBN = currentTime; //guardar o valor do timer na variável
            textHBN.text = timeHBN.ToString(); //converter o valor da variavel para UI
        }
       if (isHB[1] == true) //raiva
        {
            timeHBR = currentTime; //guardar o valor do timer na variável
            textHBR.text = timeHBR.ToString(); //converter o valor da variavel para UI
        }
       if (isHB[2] == true) //medo
        {
            timeHBM = currentTime; //guardar o valor do timer na variável
            textHBM.text = timeHBM.ToString(); //converter o valor da variavel para UI
        }
       if (isHB[3] == true) //felicidade
        {
            timeHBF = currentTime; //guardar o valor do timer na variável
            textHBF.text = timeHBF.ToString(); //converter o valor da variavel para UI
        }
        //
        //homem negro
        if (isHN[0] == true) //neutro
        {
            timeHNN = currentTime; //guardar o valor do timer na variável
            textHNN.text = timeHNN.ToString(); //converter o valor da variavel para UI
        }
        if (isHN[1] == true) //raiva
        {
            timeHNR = currentTime; //guardar o valor do timer na variável
            textHNR.text = timeHNR.ToString(); //converter o valor da variavel para UI
        }
        if (isHN[2] == true) //medo
        {
            timeHNM = currentTime; //guardar o valor do timer na variável
            textHNM.text = timeHNM.ToString(); //converter o valor da variavel para UI
        }
        if (isHN[3] == true) //felicidade
        {
            timeHNF = currentTime; //guardar o valor do timer na variável
            textHNF.text = timeHNF.ToString(); //converter o valor da variavel para UI
        }
        //
        //mulher branca
        if (isMB[0] == true) //neutro
        {
            timeMBN = currentTime; //guardar o valor do timer na variável
            textMBN.text = timeMBN.ToString(); //converter o valor da variavel para UI
        }
        if (isMB[1] == true) //raiva
        {
            timeMBR = currentTime; //guardar o valor do timer na variável
            textMBR.text = timeMBR.ToString(); //converter o valor da variavel para UI
        }
        if (isMB[2] == true) //medo
        {
            timeMBM = currentTime; //guardar o valor do timer na variável
            textMBM.text = timeMBM.ToString(); //converter o valor da variavel para UI
        }
        if (isMB[3] == true) //felicidade
        {
            timeMBF = currentTime; //guardar o valor do timer na variável
            textMBF.text = timeMBF.ToString(); //converter o valor da variavel para UI
        }
        //
        //mulher negra
        if (isMN[0] == true) //neutro
        {
            timeMNN = currentTime; //guardar o valor do timer na variável
            textMNN.text = timeMNN.ToString(); //converter o valor da variavel para UI
        }
        if (isMN[1] == true) //raiva
        {
            timeMNR = currentTime; //guardar o valor do timer na variável
            textMNR.text = timeMNR.ToString(); //converter o valor da variavel para UI
        }
        if (isMN[2] == true) //medo
        {
            timeMNM = currentTime; //guardar o valor do timer na variável
            textMNM.text = timeMNM.ToString(); //converter o valor da variavel para UI
        }
        if (isMN[3] == true) //felicidade
        {
            timeMNF = currentTime; //guardar o valor do timer na variável
            textMNF.text = timeMNF.ToString(); //converter o valor da variavel para UI
        }
        //
    }
    //

    //colocar bools a falso
    public void CleanBool()
    {
        for(int i = 0; i < isHB.Length; i++)
        {
            isHB[i] = false;
        }
        for (int i = 0; i < isHN.Length; i++)
        {
            isHN[i] = false;
        }
        for (int i = 0; i < isMB.Length; i++)
        {
            isMB[i] = false;
        }
        for (int i = 0; i < isMN.Length; i++)
        {
            isMN[i] = false;
        }
    }
    //

    //reiniciar o tempo
    public void ResetTimer()
    {
        currentTime = 0f;
    }

    //gerar ficheiro com os dados finais
    public void FileGenerator()
    {
            System.IO.File.AppendAllText(
            //System.IO.File.WriteAllText(
            /*resultspath*/buildpath,
            System.DateTime.Now + " Resultados de "
            + logininstance.inputvalue + " (em segundos): \r\n\r\n\r\n" +

            "Homem branco: \r\n\r\n" +
            "neutro: " + timeHBN + "\r\n" +
            "raiva: " + timeHBR + "\r\n" +
            "medo: " + timeHBM + "\r\n" +
            "felicidade: " + timeHBF + "\r\n\r\n\r\n" +

            "Homem negro: \r\n\r\n" +
            "neutro: " + timeHNN + "\r\n" +
            "raiva: " + timeHNR + "\r\n" +
            "medo: " + timeHNM + "\r\n" +
            "felicidade: " + timeHNF + "\r\n\r\n\r\n" +

            "Mulher branca: \r\n\r\n" +
            "neutro: " + timeMBN + "\r\n" +
            "raiva: " + timeMBR + "\r\n" +
            "medo: " + timeMBM + "\r\n" +
            "felicidade: " + timeMBF + "\r\n\r\n\r\n" +

            "Mulher negra: \r\n\r\n" +
            "neutro: " + timeMNN + "\r\n" +
            "raiva: " + timeMNR + "\r\n" +
            "medo: " + timeMNM + "\r\n" +
            "felicidade: " + timeMNF + "\r\n\r\n\r\n"
            );
    }
    ///

}
