﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEmotions : MonoBehaviour
{
    [SerializeField] private EmotionMachine emotioninstance; //buscar a emotionmachine 
    public float time; /// tempo entre bonecos
    [SerializeField] private bool stop = false; //quando deve parar de mudar os bonecos?
    [SerializeField] private Timing timingstance;
    //variáveis para tratar do random
    [SerializeField] List<string> list = new List<string>(); //lista de numeros pa random
    [SerializeField] private string bonecorandom;  ///onde sai o random
    [SerializeField] private GameObject resultados; //buscar o painel dos resultados
    [SerializeField] private bool isText = false; //quando deve parar de escrever o file?
                                                  //
    [SerializeField] private bool canClick = true; //clicou pa passar emoção?
    [SerializeField] private float clicktime; //tempo entre clicks de passar de emoção

    void Start() {
        emotioninstance = GetComponent<EmotionMachine>(); //buscar a emotionmachine
        timingstance = GetComponent<Timing>(); //buscar o script timing
        FireCoRoutines();
    }




    private void FireCoRoutines() {
        StartCoroutine(WaitingTime()); //dar tempo entre bonecos
        StartCoroutine(Clicktime()); //dar tempo entre clicks de passar de emoção
    }

    void LateUpdate() {
        Transition(); //onde gera os bonecos e transições

        //aqui vai ser o gatilho onde o tester vai clicar pa skipar
        if (canClick == true) {
            if (OVRInput.Get(OVRInput.Button.One) || Input.GetKeyDown(KeyCode.W)) {
                timingstance.ResetTimer(); //restart no timer
                Randomizer(); //randomiza de novo
                canClick = false; //não pode clicar mais
                StopAllCoroutines();
                FireCoRoutines();
            }
        }
        //
    }

    /// usado para girar os estados enum de acordo o random que sair
    void ChangeModel() {
        if (bonecorandom == "0") {
            emotioninstance.final = EmotionMachine.Final.homembranconeutro;
        }
        if (bonecorandom == "1") {
            emotioninstance.final = EmotionMachine.Final.homembrancoraiva;
        }
        if (bonecorandom == "2") {
            emotioninstance.final = EmotionMachine.Final.homembrancomedo;
        }
        if (bonecorandom == "3") {
            emotioninstance.final = EmotionMachine.Final.homembrancofeliz;
        }
        if (bonecorandom == "4") {
            emotioninstance.final = EmotionMachine.Final.homemnegroneutro;
        }
        if (bonecorandom == "5") {
            emotioninstance.final = EmotionMachine.Final.homemnegromraiva;
        }
        if (bonecorandom == "6") {
            emotioninstance.final = EmotionMachine.Final.homemnegromedo;
        }
        if (bonecorandom == "7") {
            emotioninstance.final = EmotionMachine.Final.homemnegrofeliz;
        }
        if (bonecorandom == "8") {
            emotioninstance.final = EmotionMachine.Final.mulherbrancaneutra;
        }
        if (bonecorandom == "9") {
            emotioninstance.final = EmotionMachine.Final.mulherbrancaraiva;
        }
        if (bonecorandom == "10") {
            emotioninstance.final = EmotionMachine.Final.mulherbrancamedo;
        }
        if (bonecorandom == "11") {
            emotioninstance.final = EmotionMachine.Final.mulherbrancafeliz;
        }
        if (bonecorandom == "12") {
            emotioninstance.final = EmotionMachine.Final.mulhernegraneutra;
        }
        if (bonecorandom == "13") {
            emotioninstance.final = EmotionMachine.Final.mulhernegraraiva;
        }
        if (bonecorandom == "14") {
            emotioninstance.final = EmotionMachine.Final.mulhernegramedo;
        }
        if (bonecorandom == "15") {
            emotioninstance.final = EmotionMachine.Final.mulhernegrafeliz;
        }
    }


    //inicializa quando gira e verifica quando deve parar
    void Transition() {
        if (list.Count.Equals(0)) //está vazia a lista?
        {
            //stop = false; //se sim pára
            StartCoroutine(Finish());
        }

        if (stop == true && isText == false) //está vazia a lista e falta o txt?
        {
            emotioninstance.Deactivate(); //acabar com a simulação
            list.Clear(); //limpar a lista
            resultados.SetActive(true); //ligar o painel dos resultados
            timingstance.FileGenerator(); //gerar ficheiro com os dados
            isText = true; //o txt já está feito
        }
        if (stop == false) //não é para parar?
        {
            resultados.SetActive(false); //manter o painel dos resultados off
            ChangeModel(); //girar os estados
            emotioninstance.ChooseAgent(); //buscar bonecos e emoções
            timingstance.TimerCount(); //contar o tempo de cada boneco e emoção
        }
    }

    //randomizador 
    void Randomizer() {
        bonecorandom = list[Random.Range(0, list.Count)]; //buscar valores random da lista
        list.Remove(bonecorandom); //remover valor random que saiu da lista
    }

    //interface (não é função) pa dar tempo entre bonecos e emoções
    IEnumerator WaitingTime() {
        ChangeModel();

        while (true) {
            yield return new WaitForSeconds(time);
            Randomizer();
            timingstance.ResetTimer();
        }
    }

    //interface pa dar tempo de click de passar de emoção
    IEnumerator Clicktime() {
        while (true) {
            yield return new WaitForSeconds(clicktime);
            canClick = true; //ja pode clicar
        }
    }

    IEnumerator Finish() {
        if (list.Count.Equals(0)) {
            yield return new WaitForSeconds(0);
            stop = true; //ja pode clicar
        }
    }
}


