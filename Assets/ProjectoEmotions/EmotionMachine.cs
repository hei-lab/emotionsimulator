﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionMachine : MonoBehaviour
{
    //no enum estão os nomes dos estados e bonecos
    public enum Final
    {
        homembranconeutro,
        homembrancofeliz,
        homembrancoraiva,
        homembrancomedo,
        mulherbrancaneutra,
        mulherbrancafeliz,
        mulherbrancaraiva,
        mulherbrancamedo,
        homemnegroneutro,
        homemnegrofeliz,
        homemnegromraiva,
        homemnegromedo,
        mulhernegraneutra,
        mulhernegrafeliz,
        mulhernegraraiva,
        mulhernegramedo

    }
    //

    //é a variável do enum
    public Final final;
    //

    //aqui estão guardados os modelos dos bonecos com as
    //respectivas emoções
    [SerializeField] private GameObject[] homem_branco;
    [SerializeField] private GameObject[] homem_negro;
    [SerializeField] private GameObject[] mulher_branca;
    [SerializeField] private GameObject[] mulher_negra;
    //
    [SerializeField] private Timing timingstance; //buscar o script timing
    
    void Start()
    {
        timingstance = GetComponent<Timing>(); //buscar o script timing
    }
    
    //função para seleccionar o boneco e emoção de acordo com enum.
    void BonecosEmotions()
    {
        switch(final)
        {
            case Final.homembranconeutro:
                homem_branco[0].SetActive(true); //neutro
                homem_branco[1].SetActive(false); //raiva
                homem_branco[2].SetActive(false); //medo
                homem_branco[3].SetActive(false); //felicidade
                timingstance.isHB[0] = true; //mudar bool no timing
                Debug.Log("Neutro");
                break;

            case Final.homembrancoraiva:
                homem_branco[0].SetActive(false); //neutro
                homem_branco[1].SetActive(false); //raiva
                homem_branco[2].SetActive(true); //medo
                homem_branco[3].SetActive(false); //felicidade
                timingstance.isHB[1] = true; //mudar bool no timing
                Debug.Log("Raiva");
                break;

            case Final.homembrancomedo:
                homem_branco[0].SetActive(false); //neutro
                homem_branco[1].SetActive(true); //raiva
                homem_branco[2].SetActive(false); //medo
                homem_branco[3].SetActive(false); //felicidade
                timingstance.isHB[2] = true; //mudar bool no timing
                Debug.Log("Medo");
                break;

            case Final.homembrancofeliz:
                homem_branco[0].SetActive(false); //neutro
                homem_branco[1].SetActive(false); //raiva
                homem_branco[2].SetActive(false); //medo
                homem_branco[3].SetActive(true); //felicidade
                timingstance.isHB[3] = true; //mudar bool no timing
                Debug.Log("Felicidade");
                break;

            case Final.mulherbrancaneutra:
                mulher_branca[0].SetActive(true); //neutro
                mulher_branca[1].SetActive(false); //raiva
                mulher_branca[2].SetActive(false); //medo
                mulher_branca[3].SetActive(false); //felicidade
                timingstance.isMB[0] = true; //mudar bool no timing
                Debug.Log("Neutro");
                break;

            case Final.mulherbrancaraiva:
                mulher_branca[0].SetActive(false); //neutro
                mulher_branca[1].SetActive(false); //raiva
                mulher_branca[2].SetActive(true); //medo
                mulher_branca[3].SetActive(false); //felicidade
                timingstance.isMB[1] = true; //mudar bool no timing
                Debug.Log("Raiva");
                break;

            case Final.mulherbrancamedo:
                mulher_branca[0].SetActive(false); //neutro
                mulher_branca[1].SetActive(true); //raiva
                mulher_branca[2].SetActive(false); //medo
                mulher_branca[3].SetActive(false); //felicidade
                timingstance.isMB[2] = true; //mudar bool no timing
                Debug.Log("Medo");
                break;

            case Final.mulherbrancafeliz:
                mulher_branca[0].SetActive(false); //neutro
                mulher_branca[1].SetActive(false); //raiva
                mulher_branca[2].SetActive(false); //medo
                mulher_branca[3].SetActive(true); //felicidade
                timingstance.isMB[3] = true; //mudar bool no timing
                Debug.Log("Felicidade");
                break;

            case Final.homemnegroneutro:
                homem_negro[0].SetActive(true); //neutro
                homem_negro[1].SetActive(false); //raiva
                homem_negro[2].SetActive(false); //medo
                homem_negro[3].SetActive(false); //felicidade
                timingstance.isHN[0] = true; //mudar bool no timing
                Debug.Log("Neutro");
                break;

            case Final.homemnegromraiva:
                homem_negro[0].SetActive(false); //neutro
                homem_negro[1].SetActive(false); //raiva
                homem_negro[2].SetActive(true); //medo
                homem_negro[3].SetActive(false); //felicidade
                timingstance.isHN[1] = true; //mudar bool no timing
                Debug.Log("Raiva");
                break;

            case Final.homemnegromedo:
                homem_negro[0].SetActive(false); //neutro
                homem_negro[1].SetActive(true); //raiva
                homem_negro[2].SetActive(false); //medo
                homem_negro[3].SetActive(false); //felicidade
                timingstance.isHN[2] = true; //mudar bool no timing
                Debug.Log("Medo");
                break;

            case Final.homemnegrofeliz:
                homem_negro[0].SetActive(false); //neutro
                homem_negro[1].SetActive(false); //raiva
                homem_negro[2].SetActive(false); //medo
                homem_negro[3].SetActive(true); //felicidade
                timingstance.isHN[3] = true; //mudar bool no timing
                Debug.Log("Felicidade");
                break;

            case Final.mulhernegraneutra:
                mulher_negra[0].SetActive(true); //neutro
                mulher_negra[1].SetActive(false); //raiva
                mulher_negra[2].SetActive(false); //medo
                mulher_negra[3].SetActive(false); //felicidade
                timingstance.isMN[0] = true; //mudar bool no timing
                Debug.Log("Neutro");
                break;

            case Final.mulhernegraraiva:
                mulher_negra[0].SetActive(false); //neutro
                mulher_negra[1].SetActive(false); //raiva
                mulher_negra[2].SetActive(true); //medo
                mulher_negra[3].SetActive(false); //felicidade
                timingstance.isMN[1] = true; //mudar bool no timing
                Debug.Log("Raiva");
                break;

            case Final.mulhernegramedo:
                mulher_negra[0].SetActive(false); //neutro
                mulher_negra[1].SetActive(true); //raiva
                mulher_negra[2].SetActive(false); //medo
                mulher_negra[3].SetActive(false); //felicidade
                timingstance.isMN[2] = true; //mudar bool no timing
                Debug.Log("Medo");
                break;

            case Final.mulhernegrafeliz:
                mulher_negra[0].SetActive(false); //neutro
                mulher_negra[1].SetActive(false); //raiva
                mulher_negra[2].SetActive(false); //medo
                mulher_negra[3].SetActive(true); //felicidade
                timingstance.isMN[3] = true; //mudar bool no timing
                Debug.Log("Felicidade");
                break;
        }
    }

    //função final pa desactivar os modelos anteriores (pa evitar bugs) e
    //chamar os bonecos com a respectiva emoção
    public void ChooseAgent()
    {
        switch(final)
        {
            case Final.homembranconeutro:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem branco neutro");
                break;

            case Final.homembrancoraiva:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem branco raiva");
                break;

            case Final.homembrancomedo:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem branco medo");
                break;

            case Final.homembrancofeliz:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem branco feliz");
                break;

            case Final.homemnegroneutro:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem negro neutro");
                break;

            case Final.homemnegromraiva:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem negro raiva");
                break;

            case Final.homemnegromedo:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem negro medo");
                break;

            case Final.homemnegrofeliz:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("Homem negro feliz");
                break;

            case Final.mulherbrancaneutra:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher branca neutro");
                break;

            case Final.mulherbrancaraiva:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher branca raiva");
                break;

            case Final.mulherbrancamedo:
                Deactivate(); //desactiva agentes anteriores 
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher branca medo");
                break;

            case Final.mulherbrancafeliz:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher branca feliz");
                break;

            case Final.mulhernegraneutra:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher negra neutra");
                break;

            case Final.mulhernegraraiva:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher negra neutra");
                break;

            case Final.mulhernegramedo:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher negra neutra");
                break;

            case Final.mulhernegrafeliz:
                Deactivate(); //desactiva agentes anteriores
                BonecosEmotions(); //activa boneco e emoção
                Debug.Log("mulher negra feliz");
                break;
        }
    }

    
    /*função que desactiva os bonecos (usado pa desactivar os bonecos
    anteriores antes de passar para o próximo pa evitar bugs).*/
    public void Deactivate()
    {
        /* aqui todos os agentes são desactivados
         */
        foreach (GameObject _obj in homem_branco)
        {
            _obj.SetActive(false);
        }
        foreach (GameObject _obj in homem_negro)
        {
            _obj.SetActive(false);
        }
        foreach (GameObject _obj in mulher_branca)
        {
            _obj.SetActive(false);
        }
        foreach (GameObject _obj in mulher_negra)
        {
            _obj.SetActive(false);
        }
        timingstance.CleanBool(); //colocar bools a falso
        Debug.Log("Desactivando");
    }

}
