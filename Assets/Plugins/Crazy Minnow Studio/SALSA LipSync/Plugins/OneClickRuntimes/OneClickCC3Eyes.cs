﻿using UnityEngine;
using System.Text.RegularExpressions;

namespace CrazyMinnow.SALSA.OneClicks
{
	public class OneClickCC3Eyes : MonoBehaviour
	{
		public static void Setup(GameObject go)
		{
			string head = "head";
			string[] body = new string[] {"body", "^genesis([238])?(fe)?(male)?.shape_.$"};
			string[] eyeL = new string[] {"l_eye", "leye"};
			string[] eyeR = new string[] {"r_eye", "reye"};
			Transform eyelashTrans;
			SkinnedMeshRenderer eyelashSMR;
			string[] eyelashes = new string[] {"^genesis([238])?(fe)?(male)?eyelashes.shape_.$"};
			string[] blinkL = new string[] {"blink_l", "eye_blink_l"};
			string[] blinkR = new string[] {"blink_r", "eye_blink_r"};

			if (go)
			{
				Eyes eyes = go.GetComponent<Eyes>();
				if (eyes == null)
				{
					eyes = go.AddComponent<Eyes>();
				}
				else
				{
					DestroyImmediate(eyes);
					eyes = go.AddComponent<Eyes>();
				}
				QueueProcessor qp = go.GetComponent<QueueProcessor>();
				if (qp == null) qp = go.AddComponent<QueueProcessor>();

				// System Properties
                eyes.characterRoot = go.transform;
                eyes.queueProcessor = qp;

                // Heads - Bone_Rotation
                eyes.BuildHeadTemplate(Eyes.HeadTemplates.Bone_Rotation_XY);
                eyes.heads[0].expData.name = "head";
                eyes.heads[0].expData.components[0].name = "head";
                eyes.heads[0].expData.controllerVars[0].bone = Eyes.FindTransform(eyes.characterRoot, head);
                eyes.headTargetOffset.y = 0.052f;
                eyes.FixAllTransformAxes(ref eyes.heads, false);
                eyes.FixAllTransformAxes(ref eyes.heads, true);

                // Eyes - Bone_Rotation
                eyes.BuildEyeTemplate(Eyes.EyeTemplates.Bone_Rotation);
                eyes.eyes[0].expData.name = "eyeL";
                eyes.eyes[0].expData.components[0].name = "eyeL";
                eyes.eyes[0].expData.controllerVars[0].bone = Eyes.FindTransform(eyes.characterRoot, eyeL);
                eyes.eyes[1].expData.name = "eyeR";
                eyes.eyes[1].expData.components[0].name = "eyeR";
                eyes.eyes[1].expData.controllerVars[0].bone = Eyes.FindTransform(eyes.characterRoot, eyeR);
                eyes.FixAllTransformAxes(ref eyes.eyes, false);
                eyes.FixAllTransformAxes(ref eyes.eyes, true);

                // Eyelids - Bone_Rotation
                eyes.BuildEyelidTemplate(Eyes.EyelidTemplates.BlendShapes); // includes left/right eyelid
                eyes.SetEyelidShapeSelection(Eyes.EyelidSelection.Upper);
                float blinkMax = 1f;
                // Left eyelid
                eyes.eyelids[0].expData.name = "eyelidL";
                eyes.eyelids[0].referenceIdx = 0;
                eyes.eyelids[0].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot,  body).GetComponent<SkinnedMeshRenderer>();
                eyes.eyelids[0].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[0].smr, blinkL);
                eyes.eyelids[0].expData.controllerVars[0].maxShape = blinkMax;
                // Right eyelid
                eyes.eyelids[1].expData.name = "eyelidR";
                eyes.eyelids[1].referenceIdx = 1;
                eyes.eyelids[1].expData.controllerVars[0].smr = eyes.eyelids[0].expData.controllerVars[0].smr;
                eyes.eyelids[1].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[0].smr, blinkR);
                eyes.eyelids[1].expData.controllerVars[0].maxShape = blinkMax;
                // Used when DAZ is imported into iClone
                eyelashTrans = Eyes.FindTransform(eyes.characterRoot, eyelashes);
                if (eyelashTrans != null)
                {
	                eyelashSMR = eyelashTrans.GetComponent<SkinnedMeshRenderer>();
	                if (eyelashSMR != null)
	                {
		                eyes.AddEyelidShapeExpression(); // left
		                eyes.eyelids[2].expData.name = "eyelashL";
		                eyes.eyelids[2].referenceIdx = 0;
		                eyes.eyelids[2].expData.controllerVars[0].smr = eyelashSMR;
		                eyes.eyelids[2].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyelashSMR, blinkL);
		                eyes.eyelids[2].expData.controllerVars[0].maxShape = blinkMax;
		                
		                eyes.AddEyelidShapeExpression(); // right
		                eyes.eyelids[3].expData.name = "eyelashR";
		                eyes.eyelids[3].referenceIdx = 1;
		                eyes.eyelids[3].expData.controllerVars[0].smr = eyelashSMR;
		                eyes.eyelids[3].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyelashSMR, blinkR);
		                eyes.eyelids[3].expData.controllerVars[0].maxShape = blinkMax;
	                }
                }

                // Initialize the Eyes module
                eyes.Initialize();
			}
		}
	}
}